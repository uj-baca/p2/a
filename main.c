#include <stdio.h>
#include <stdlib.h>

struct __cube {
    int sizeX, sizeY, sizeZ;
    int startX, startY, startZ;
    int directionX, directionY, directionZ;
};

struct __object {
    int x, y, z;
    int directionX, directionY, directionZ;
};

typedef struct __cube *Cube;
typedef struct __object *Object;

int main() {
    int count, i = 0;
    scanf("%d", &count);
    for (; i < count; i++) {
        int simulations, interval;
        Cube cube = (Cube) malloc(sizeof(struct __cube));
        scanf("%d%d%d", &(cube->sizeX), &(cube->sizeY), &(cube->sizeZ));
        scanf("%d%d%d", &(cube->startX), &(cube->startY), &(cube->startZ));
        scanf("%d%d%d", &(cube->directionX), &(cube->directionY), &(cube->directionZ));
        scanf("%d", &simulations);
        scanf("%d", &interval);
        char *indexes = (char*) malloc(cube->sizeX + cube->sizeY + cube->sizeZ + 3);
        scanf("%s", indexes);
        if (cube->directionX == 0 && cube->directionY == 0 && cube->directionZ == 0) {
            return 0;
        }
        int j = 0;
        int objectCounter = 0;
        Object *o = (Object*) malloc(sizeof(struct __object) * (simulations / interval + (simulations % interval > 0 ? 1 : 0)));
        for (; j < simulations; j++) {
            if (j % interval == 0) {
                *(o + objectCounter) = (Object) malloc(sizeof(struct __object));
                Object tmp = *(o + objectCounter);
                ++objectCounter;
                tmp->x = cube->startX;
                tmp->y = cube->startY;
                tmp->z = cube->startZ;
                tmp->directionX = cube->directionX;
                tmp->directionY = cube->directionY;
                tmp->directionZ = cube->directionZ;
            }
            int c = 0;
            for (; c < objectCounter; c++) {
                Object tmp = *(o + c);
                int ping = 0;
                if (tmp->x + tmp->directionX > cube->sizeX || tmp->x + tmp->directionX < 0) {
                    tmp->directionX *= -1;
                    ping = 1;
                }
                if (tmp->y + tmp->directionY > cube->sizeY || tmp->y + tmp->directionY < 0) {
                    tmp->directionY *= -1;
                    ping = 1;
                }
                if (tmp->z + tmp->directionZ > cube->sizeZ || tmp->z + tmp->directionZ < 0) {
                    tmp->directionZ *= -1;
                    ping = 1;
                }
                if (ping) {
                    printf("#%d:%c%c%c", j, *(indexes + tmp->x), *(indexes + cube->sizeX + 1 + tmp->y),
                           *(indexes + cube->sizeX + cube->sizeY + 2 + tmp->z));
                }
                tmp->x += tmp->directionX;
                tmp->y += tmp->directionY;
                tmp->z += tmp->directionZ;
            }
        }
        free(o);
        free(cube);
    }
    printf("\n");
    return 0;
}
